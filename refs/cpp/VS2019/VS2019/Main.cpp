#include <iostream>
#include <stdlib.h> // srand, rand
#include <time.h> // time
#include <vector>
#include <algorithm> // sort
#include <deque>
#include <unordered_map>
#include "Main.h"
#include <map>

using namespace std;

// General notes:
// In C++, functions can live outside of objects. These are called global functions.
// C++ has no garbage collector. Memory management is manual and memory leaks are possible.
// C++ generally has lower memory usage than Java.
// C++ templates correspond to Java generics.

// resources:
// https://www.learncpp.com/
// http://www.cplusplus.com/doc/tutorial/
// https://en.cppreference.com/w/

// Suggestion for project layout: https://api.csswg.org/bikeshed/?force=1&url=https://raw.githubusercontent.com/vector-of-bool/pitchfork/develop/data/spec.bs

// Suggestions for header files: https://www.learncpp.com/cpp-tutorial/89-class-code-and-header-files/

// Disabling address space layout randomization? https://stackoverflow.com/questions/9560993/how-do-you-disable-aslr-address-space-layout-randomization-on-windows-7-x64

void section(string s) { cout << "\n**********\n* " + s + "\n**********\n\n"; }
void printDequeInt(string s, deque<int> d) { cout << s; for (int i = 0; i < d.size(); i++) { cout << d[i] << ","; } cout << endl; }
bool sort_reverse(int a, int b) { return (a > b); }

int main()
{
	cout << "Hello World\n";


	//**********
	section("variable initialization");
	//**********

	int a = 1; // copy
	int b(2);  // direct
	int c{ 3 }; // uniform
	cout << c << endl;

	float f1 = 0.2; // warning: truncation from double to float
	float f2 = 0.2f;
	double d1 = 0.3;
	cout << (double)f1 * (double)f2 * d1 << endl;

	int nine = 0b1001; // supported in C++14
	int twentyTwo = 0x16;
	cout << nine + twentyTwo << endl;

	//**********
	section("array");
	//**********

	// different ways to initialize
	int arry1a[2];
	arry1a[0] = 55;
	arry1a[1] = 66;
	cout << arry1a[1] << endl;

	int arry1b[2]{ 88, 99 };
	cout << arry1b[1] << endl;

	int arry1c[]{ 9, 8 };
	cout << arry1c[1] << endl;

	// array vars are fixed, pointer vars can change
	int arry10[]{ 50, 51 };
	int arry11[]{ 60, 61 };
	int* iPtr1 = arry10;
	cout << arry10[0] << " " << arry11[0] << " " << iPtr1[0] << endl;
	iPtr1 = arry11;
	cout << arry10[0] << " " << arry11[0] << " " << iPtr1[0] << endl;

	// static array allocation (must use array var?)
	int arry3a[]{ 18, 19, 20 };
	cout << "size of arry3a = " << sizeof(arry3a) << endl; // 12 (3 ints * 4 bytes per int)

	// dynamic array allocation (must use pointer var?)
	// http://www.fredosaurus.com/notes-cpp/newdelete/50dynamalloc.html

	srand(time(NULL));
	int iRand = rand() % 2 + 2;
	int* arry3b = new int[iRand];
	for (int i = 0; i < iRand; i++) { arry3b[i] = rand(); }
	cout << arry3b[1] << endl;
	cout << "size of arry3b = " << sizeof(arry3b) << endl; // 8 (8 bytes per pointer on 64-bit arch)
	delete[] arry3b;

	// dynamic alloc with malloc
	int* arry4a = (int*)malloc(sizeof(int) * 3); // returns NULL on failure
	if (arry4a != NULL)
	{
		arry4a[0] = 15;
		arry4a[1] = 16;
		arry4a[2] = 17;
		cout << arry4a[1] << endl;
	}
	cout << "size of arry4a = " << sizeof(arry4a) << endl; // 8 (8 bytes per pointer on 64-bit arch)
	free(arry4a);

	// dynamic alloc with new
	int* arry4b = new int[3]; // throws exception on failure
	arry4b[0] = 21;
	arry4b[1] = 22;
	arry4b[2] = 23;
	cout << arry4b[1] << endl;
	cout << "size of arry4b = " << sizeof(arry4b) << endl; // 8 (8 bytes per pointer on 64-bit arch)
	delete[] arry4b;

	//**********
	section("vector");
	//**********

	vector<int> v1(3);
	v1[0] = 22;
	v1[1] = 33;
	v1[2] = 44;
	//v1[8] = 55; // succeeds?! but what does it do internally? update: maybe not, only succeeds in Release?
	cout << v1[1] << endl;

	vector<int> v2{ 55, 66, 77 };
	cout << v2[1] << endl;

	// iterate with range-for
	for (auto x : v2) { cout << x << ","; }
	cout << endl;

	// iterate with iterator
	for (auto iter = v2.begin(); iter != v2.end(); iter++) { cout << *iter << ","; }
	cout << endl;

	// iterate and change with iterator
	for (auto iter = v2.begin(); iter != v2.end(); iter++) { (*iter)++; } // careful, ++ has higher precedence than *, so force * first
	cout << endl;

	// iterate and change with range-for
	//for (auto x : v2) { x++; } // doesn't work, only increments copy of element
	for (auto& x : v2) { x++; }
	for (auto x : v2) { cout << x << ","; }
	cout << endl;

	// filter by creating a copy
	vector<int> v3;
	copy_if(v2.begin(), v2.end(), back_inserter(v3), [](int x) { return x < 70; });
	for (auto x : v3) { cout << x << ","; }
	cout << endl;

	// how to filter stl container?
	// https://stackoverflow.com/questions/21204676/modern-way-to-filter-stl-container

	// maybe for now, cleanest way to filter a vector is to iterate and populate a copy vector with good elements.
	// or, use a list?

	// remember, the difficulty which must be overcome when filtering vectors
	// is that c++ requires vectors to be laid out continguously in memory.
	// this means that deletes must shift all remaining elements.


	//**********
	section("algorithm lib functions on vectors");
	//**********

	sort(v1.begin(), v1.end());
	bool present = binary_search(v1.begin(), v1.end(), 33);
	cout << 33 << " present: " << present << endl;

	// reverse sort
	sort(v1.begin(), v1.end(), sort_reverse);
	for (auto x : v1) { cout << x << ","; }
	cout << endl;

	// lower_bound
	// upper_bound

	// todo: sort a vector of objects/structs

	//**********
	section("list");
	//**********

	//**********
	section("strings");
	//**********

	cout << "first string\n";

	string s1 = "hello";
	string s2 = "there";
	string s3 = s1 + " " + s2;
	cout << s3 << endl;

	// char array to string.
	char cArry1[]{ 'h', 'i', '\0' };
	cout << string(cArry1) << endl;

	// string to char array.

	//strcpy(cArry2a, s3.c_str()); // error: unsafe
	//strncpy(cArry2c, s3.c_str(), s3.size()); // error: unsafe

	char cArry2a[20];
	strcpy_s(cArry2a, sizeof(cArry2a), s3.c_str()); // null terminates after copy. if string doesn't fit in array, invokes invalid param handler.
	cout << string(cArry2a) << endl;

	char* cArry2b = new char[s3.size() + 1];
	strcpy_s(cArry2b, sizeof(char) * (s3.size() + 1), s3.c_str());
	cout << string(cArry2b) << endl;



	//**********
	section("queue");
	//**********

	deque<int> dq1{ 4, 5 };
	dq1.push_front(3);
	dq1.push_back(6);
	cout << dq1.front() << "," << dq1.back() << endl;
	dq1.pop_front();
	dq1.pop_back();

	printDequeInt("dq1 = ", dq1);


	//**********
	section("hashmap"); // in c++, hashmap is available as an unordered_map
	//**********

	unordered_map<int, string> um1{
		{1, "foo"},
		{2, "bar"},
		{10, "bap"},
		{11, "bop"},
		{12, "hop"},
		{13, "hip"}
	};
	cout << um1[2] << endl;

	// insert
	um1[3] = "baz";

	// alternate insert
	um1.insert(make_pair(4, "bif"));

	//
	um1.insert(pair<int, string>(15, "bit"));


	// delete by key
	um1.erase(4);
	um1.erase(5); // does not exist

	// delete by iterator
	um1.erase(um1.begin());
	um1.erase(um1.find(12));

	// contains key
	if (um1.find(10) != um1.end()) { cout << "found key " << 10 << endl; }
	else { cout << "notfound key " << 10 << endl; }
	if (um1.find(20) != um1.end()) { cout << "found key " << 20 << endl; }
	else { cout << "notfound key " << 20 << endl; }

	// iterate with iterator
	for (auto iter = um1.begin(); iter != um1.end(); iter++) { cout << iter->first << ":" << iter->second << " "; }
	cout << endl;

	// iterate by reference
	for (auto& x : um1){std::cout << x.first << ": " << x.second << std::endl;} // iterating by reference allows me to change val?
		

	// filter (must use iterator)
	for (auto iter = um1.begin(); iter != um1.end();) // CAUTION: do not do ++ here, you will skip an elt after delete.
	{
		if (iter->second[1] == 'a') { iter = um1.erase(iter); }
		else { iter++; }
	}
	cout << "after filter:" << endl;
	for (auto& x : um1) { std::cout << x.first << ":" << x.second << " "; }
	cout << endl;


	//**********
	section("tree"); // (can be map (key-value) or set (key))
	//**********

	map<int, string> map1{
		{1, "foo"},
		{2, "bar"}
	};

	// insert
	map1[3] = "baz";
	map1.insert(make_pair(8, "bap"));

	// delete
	map1.erase(8);

	// iterate

	for (auto iter = map1.begin(); iter != map1.end(); iter++) { cout << iter->first << ":" << iter->second << " "; }
	cout << endl;

	// iterate with update

	// iterate with delete (filter)
	// carefully consider 2 approaches given by first answer here: https://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it

	//**********
	section("classes"); // and inheritance
	//**********

	// Box2D defined in header

	// managing vectors of classes
	vector<Box2D> vBox2D1{
		Box2D(1, 2, 3, 4),
		Box2D(2, 3, 4, 5),
		Box2D(3, 4, 5, 6)
	};

	//vector<Box2D>::iterator box2DIter;
	for (auto box2DIter = vBox2D1.begin(); box2DIter != vBox2D1.end(); box2DIter++)
	{
		// CAUTION: this does not update the object.
		//Box2D curr = *box2DIter; // creates a copy
		//curr.centerX++;

		box2DIter->centerX++;
	}
	cout << vBox2D1[0].asString() << endl;

	// iterate using references?
	//for (auto& curr = vBox2D1.begin(); curr != vBox2D1.end(); curr++) // doesn't work
	//{
	//	//
	//}

	// one disappointing feature: it is possible to loop over vectors/maps/deques/etc with range for loop,
	// but there is no way to do deletes with a range for loop.
	// https://stackoverflow.com/questions/10360461/removing-item-from-vector-while-in-c11-range-for-loop
	// for this reason, i think i prefer iterator loops, even though you can't use them to access objects by reference.

	// hm, but if i need deletes then i shouldn't be using vectors anyways?
	// vectors must be contiguous, so i assume delete shifts all remaining items?
	// or use copy_if? http://www.cplusplus.com/reference/algorithm/copy_if/
	// also investigate remove_copy_if: https://en.cppreference.com/w/cpp/algorithm/remove
	// remove_if?

	//**********
	// structs
	//**********

	// how to sort a vector of structs

	//**********
	// padding
	//**********

	//**********
	// new and del vs malloc and free
	//**********

	// new calls the class constructor.

	//**********
	// threading and async?
	//**********

	//**********
	// exceptions
	//**********

	//**********
	// overloading
	//**********

	//**********
	// regex (put with strings?)
	//**********

	system("pause");
	return 0;
}