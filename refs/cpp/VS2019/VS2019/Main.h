
#pragma once

#include <string>

class Box2D
{
public:
	int centerX;
	int centerY;
	int width;
	int height;

	Box2D(int x, int y, int w, int h) :centerX(x), centerY(y), width(w), height(h) {}
	~Box2D() {}

	std::string asString() { 
		return std::to_string(centerX) + "," + std::to_string(centerY) + "," + std::to_string(width) + "," + std::to_string(height);
	}
};