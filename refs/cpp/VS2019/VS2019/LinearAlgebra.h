#pragma once


class Matrix
{
private:
	int m; // num rows
	int n; // num cols
	double* A;

public:

	Matrix(int m, int n);
	~Matrix();

	int numRows() { return m; }
	int numCols() { return n; }

	static Matrix MatMul(Matrix A, Matrix B);
};

