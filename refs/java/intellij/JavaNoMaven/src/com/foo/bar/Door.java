package com.foo.bar;

public abstract class Door {
    abstract void makeSecure();
}
