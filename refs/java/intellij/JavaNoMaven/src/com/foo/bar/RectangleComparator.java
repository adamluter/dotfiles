package com.foo.bar;

import java.util.Comparator;

public class RectangleComparator implements Comparator<Rectangle> {
    public int compare(Rectangle a, Rectangle b){
        int areaA = a.w * a.h;
        int areaB = b.w * b.h;
        if(areaA > areaB){ return 1; }
        else if(areaA < areaB){ return -1; }
        else {return 0; }
    }
}
