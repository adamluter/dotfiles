package com.foo.bar;

public class CellPhone implements MsgSender, MsgReceiver {
    public void send(){
        System.out.println("cellphone sending 'hello'");
    }

    public void receive(){
        System.out.println("cellphone receiving 'sup?'");
    }
}
