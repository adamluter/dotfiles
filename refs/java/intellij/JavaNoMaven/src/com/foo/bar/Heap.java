package com.foo.bar;

import java.util.ArrayList;
import java.util.LinkedList;

public class Heap<K extends Comparable> {
    // Why not "implements Comparable"?
    // https://stackoverflow.com/questions/976441/java-generics-why-is-extends-t-allowed-but-not-implements-t
    // https://stackoverflow.com/questions/18819068/why-cant-a-java-generic-implement-an-interface?noredirect=1&lq=1
    // Generics are also called type parameters?
    // Oracle docs: https://docs.oracle.com/javase/tutorial/java/generics/bounded.html
    // "If one of the bounds is a class, it must be specified first."
    // I assume they are using "extends" in a 2nd sense here, as an umbrella term for "extends" and "implements".
    // Probably to avoid type parameters to have both "extends" and "implements" keywords.

    public static enum Direction{
        MIN,
        MAX
    }

    private ArrayList<K> arry;
    private Direction dir;
    private int idx;
    private int rootIdx = 1;
    private int leftChildIdx(int a){ return 2 * a; }
    private int rightChildIdx(int a){ return 2 * a + 1; }
    private int parentIdx(int a){ return a / 2; }

    public Heap(Direction d){
        dir = d;
        arry = new ArrayList<K>();
        arry.add(null);
        idx = 1;
    }

    private boolean problem(int a){
        if(dir == Direction.MIN){
            if(arry.get(a).compareTo(arry.get(parentIdx(a))) < 0){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            if(arry.get(a).compareTo(arry.get(parentIdx(a))) > 0){
                return true;
            }
            else{
                return false;
            }
        }
    }

    private boolean problemChild(int a){
        if(dir == Direction.MIN){
            if(
                    (leftChildIdx(a) < idx && arry.get(a).compareTo(arry.get(leftChildIdx(a))) > 0)
                    || (rightChildIdx(a) < idx &&arry.get(a).compareTo(arry.get(rightChildIdx(a))) > 0)
            ){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            if(
                    (leftChildIdx(a) < idx && arry.get(a).compareTo(arry.get(leftChildIdx(a))) < 0)
                    || (rightChildIdx(a) < idx &&arry.get(a).compareTo(arry.get(rightChildIdx(a))) < 0)
            ){
                return true;
            }
            else{
                return false;
            }
        }
    }

    private void swap(int a, int b){
        K tmp = arry.get(a);
        arry.set(a, arry.get(b));
        arry.set(b, tmp);
    }

    private int chooseSwapTarget(int a){
        // We are guaranteed by childProblem() that at least left child exists.
        if(dir == Direction.MIN){
            if(rightChildIdx(a) < idx && arry.get(leftChildIdx(a)).compareTo(arry.get(rightChildIdx(a))) > 0){
                return rightChildIdx(a);
            }
            else{
                return leftChildIdx(a);
            }
        }
        else{
            if(rightChildIdx(a) < idx && arry.get(leftChildIdx(a)).compareTo(arry.get(rightChildIdx(a))) < 0){
                return rightChildIdx(a);
            }
            else{
                return leftChildIdx(a);
            }
        }
    }

    public void insert(K k){
        if(arry.size() <= idx){
            arry.add(k);
        }
        else{
            arry.set(idx, k);
        }
        int curr = idx;
        idx++;

        while(curr != rootIdx && problem(curr)){
            swap(curr, parentIdx(curr));
            curr = parentIdx(curr);
        }
    }

    public K remove(){
        K ret = arry.get(rootIdx);
        idx--;
        arry.set(rootIdx, arry.get(idx));
        int curr = rootIdx;
        while(curr < idx && problemChild(curr)){
            // To decide between swapping left and right, we must know which is lesser.
            // If there is a problem, we will swap with the lesser child.
            int swapTarget = chooseSwapTarget(curr);
            swap(curr, swapTarget);
            curr = swapTarget;
        }
        return ret;
    }

    public K peek(){
        return arry.get(rootIdx);
    }

    public int size(){
        return idx - 1;
    }

    public LinkedList<K> destructiveSort(){
        return null;
    }
}
